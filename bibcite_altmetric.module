<?php

/**
 * @file
 * Module hooks.
 */

use Drupal\bibcite_entity\Entity\Reference;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_entity_extra_field_info().
 */
function bibcite_altmetric_entity_extra_field_info() {
  $extra = [];

  $storage = \Drupal::entityTypeManager()->getStorage('bibcite_reference_type');
  foreach ($storage->loadMultiple() as $bundle) {
    $extra['bibcite_reference'][$bundle->id()]['display']['bibcite_altmetric'] = [
      'label' => t('Altmetric badge'),
      'description' => t('Show Altmetric badge'),
      'weight' => 150,
    ];
  }

  return $extra;
}

/**
 * Implements hook_bibcite_reference_view_alter().
 */
function bibcite_altmetric_bibcite_reference_view_alter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display) {
  if ($entity instanceof Reference) {
    $config = \Drupal::config('bibcite_altmetric.settings');
    $sources = $config->get('sources');
    $source = NULL;

    $alt_sources = [
      'DOI' => 'data-doi',
      'ISBN' => 'data-isbn',
      'URI' => 'data-uri',
      'PubMed' => 'data-pmid',
      'arXiv' => 'data-arxiv-id',
      'Handle' => 'data-handle',
      'URN' => 'data-urn',
      'NCT' => 'data-nct-id',
      'Altmetric' => 'data-altmetric-id',
    ];

    $badge_views = [
      'SmallDonut' => 'donut',
      'MediumDonut' => 'medium-donut',
      'LargeDonut' => 'large-donut',
      'SmallBar' => 'bar',
      'MediumBar' => 'medium-bar',
      'LargeBar' => 'large-bar',
      'SmallBadge' => '4',
      'MediumBadge' => '2',
      'LargeBadge' => '1',
    ];

    $table_views = [
      'Table right' => ['key' => 'data-badge-details', 'value' => 'right'],
      'Popover top' => ['key' => 'data-badge-popover', 'value' => 'top'],
      'Popover right' => ['key' => 'data-badge-popover', 'value' => 'right'],
      'Popover bottom' => ['key' => 'data-badge-popover', 'value' => 'bottom'],
      'Popover left' => ['key' => 'data-badge-popover', 'value' => 'left'],
    ];

    foreach ($sources as $source_id => $field_id) {
      if ($field_id && $field_id['field']) {
        /** @var \Drupal\Core\Field\FieldItemList $field */
        $field = $entity->get($field_id['field']);
        $value = $field->getValue();
        if ($value) {
          $source = $alt_sources[$source_id];
          break;
        }
      }
    }
    $badge = $config->get('badge');

    if (isset($value) && $source && $badge && $component = $display->getComponent('bibcite_altmetric')) {
      $size = $config->get('size');
      $condensed = $config->get('condensed') ?: FALSE;
      $details = $config->get('details') ?: NULL;
      $scores = $config->get('show_scores');
      $new_tab = $config->get('new_tab');

      $build['bibcite_altmetric'] = [
        '#type' => 'altmetric',
        '#weight' => $component['weight'],
        '#badge' => $badge_views[$size . $badge],
        '#source' => $source,
        '#source_value' => $value[0]['value'],
        '#condensed' => $condensed ? 'true' : 'false',
      ];
      if ($details) {
        $build['bibcite_altmetric']['#details'] = $table_views[$details];
      }
      if (!$scores) {
        $build['bibcite_altmetric']['#no_score'] = TRUE;
      }
      if ($new_tab) {
        $build['bibcite_altmetric']['#new_tab'] = TRUE;
      }
    }
  }
}
