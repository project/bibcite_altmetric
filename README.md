CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Altmetric license
* Maintainers


INTRODUCTION
------------

 **Bibliography & Citation - Altmetric**
 Adds [Altmetric](https://www.altmetric.com/) badges to [BibCite reference entities](https://drupal.org/project/bibcite).

 * For a full description of the module, visit the project page:
   https://drupal.org/project/bibcite_altmetric
 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/bibcite_altmetric


REQUIREMENTS
------------

This module requires the following modules:
 * [Bibliography & Citation](https://www.drupal.org/project/bibcite)


INSTALLATION
------------

 * Install required contributed modules.
 * Install this module as you would normally install a contributed Drupal module.

See: https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules
for further information.


ALTMETRIC LICENSE
-----------------

Altmetric badges are free for individual researchers to display on their
personal academic websites. Institutional repositories are also permitted to
display Altmetric badges for free. Academic departments, academic websites, and
subject repositories require a license. Please see
[https://www.altmetric.com/products/altmetric-badges/](https://www.altmetric.com/products/altmetric-badges/)
for more information.


MAINTAINERS
-----------

Current maintainers:
 * Anton Shubkin (antongp) - https://www.drupal.org/u/antongp
 * adci_contributor - https://www.drupal.org/u/adci_contributor

This project has been sponsored by [ADCI Solutions](https://www.adcisolutions.com/)
